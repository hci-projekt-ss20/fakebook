import React from 'react'
import { makeStyles, ThemeProvider } from '@material-ui/core/styles'
import {
  CssBaseline,
  Container,
  Grid,
} from '@material-ui/core'
import theme from './theme'
import Header from './components/Header'
import Menu from './components/Menu'
import Posts from './components/Posts'
import Contacts from './components/Contacts'
import EvaluationConductor from './components/EvaluationConductor'
import TopNavigation from './components/TopNavigation'
import { contactsOnline, contactsAway, posts } from './utils/contentGenerators'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    marginBottom: '1em',
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}))

const App = () => {
  const classes = useStyles()

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container maxWidth="lg">
        <div className={classes.root}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Header />
              <TopNavigation />
            </Grid>
            <Grid item xs={12} md={3}>
              <Menu className={classes.paper}/>
              <EvaluationConductor
                contactsOnline={contactsOnline}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <Posts posts={posts} />
            </Grid>
            <Grid item xs={12} md={3}>
              <Contacts
                className={classes.paper}
                contactsOnline={contactsOnline}
                contactsAway={contactsAway}
              />
            </Grid>
          </Grid>
        </div>
      </Container>
    </ThemeProvider>
  )
}

export default App
