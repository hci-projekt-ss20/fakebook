import React from 'react'
import {
  MenuList,
  MenuItem,
  Paper,
  ListItemIcon,
  ListItemText,
  Typography
} from '@material-ui/core'
import PersonIcon from '@material-ui/icons/Person'
import { makeStyles, useTheme } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
  },
  h1: {
    fontWeight: 500,
  },
}))

const Contacts = ({ contactsOnline, contactsAway }) => {
  const theme = useTheme()
  const classes = useStyles()

  return (
    <Paper className={classes.paper}>
      <Typography variant="h5">Contacts</Typography>
      <MenuList>
        {contactsOnline.map((contact, index) =>
          <MenuItem key={index} id={`contact-${contact}`}>
            <ListItemIcon>
              <PersonIcon style={{ color: theme.palette.contactOnline }}/>
            </ListItemIcon>
            <ListItemText inset primary={contact}/>
          </MenuItem>
        )}
        {contactsAway.map((contact, index) =>
          <MenuItem key={index} id={`contact-${contact}`}>
            <ListItemIcon>
              <PersonIcon style={{ color: theme.palette.contactAway }}/>
            </ListItemIcon>
            <ListItemText inset primary={contact}/>
          </MenuItem>
        )}
      </MenuList>
    </Paper>
  )
}

export default Contacts
