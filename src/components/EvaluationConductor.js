import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Paper,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Typography,
} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  paper: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
    padding: '0 1rem',
  },
  h1: {
    fontWeight: 500,
  },
}))

const EvaluationConductor = ({ contactsOnline }) => {
  const classes = useStyles()

  const [testRunning, setTestRunning] = useState(false)
  const [modalOpen, setModalOpen] = useState(false)

  const [testStartedAt, setTestStartedAt] = useState(undefined)
  const [evaluationResults, setEvaluationResults] = useState(undefined)

  const targetContact = contactsOnline[Math.floor(Math.random() * contactsOnline.length)]

  const startTest = () => {
    setModalOpen(false)
    setTestRunning(true)
    setTestStartedAt(new Date())
    document.getElementById(`contact-${targetContact}`).addEventListener('click', () => stopTest())
  }

  const stopTest = () => {
    setTestRunning(false)
    setEvaluationResults({
      time: new Date() - testStartedAt
    })
  }

  return (
    <Paper className={classes.paper}>
      <Dialog fullScreen
        open={modalOpen}
        onClose={() => startTest()}
      >
        <div style={{ width: '32em', margin: 'auto', border: '1px solid black' }}>
          <DialogTitle>
            Task
          </DialogTitle>
          <DialogContent>
            Click on {targetContact} in the online contacts sidebar.
          </DialogContent>
          <DialogActions>
            <Button onClick={() => startTest()}>
              start
            </Button>
          </DialogActions>
        </div>
      </Dialog>
      <Typography variant="h5">Evaluation</Typography>
      {
        testRunning ?
          <Button onClick={() => stopTest()}>Stop</Button>
          :
          <Button onClick={() => setModalOpen(true)}>Start</Button>
      }
      {evaluationResults ?
        <>
          <Typography variant="h6">Test results</Typography>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>Time</TableCell>
                <TableCell>{evaluationResults.time / 1000} seconds</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </>
        :
        ''}
    </Paper>
  )
}

export default EvaluationConductor
