import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Paper, Typography } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  paper: {
    backgroundColor: theme.palette.primary.dark,
    border: 0,
    borderRadius: 0,
    color: theme.palette.common.white,
    padding: '0 1rem',
  },
  h1: {
    fontWeight: 500,
  },
}))


const Header = () => {
  const classes = useStyles()

  return (
    <Paper className={classes.paper} elevation={5}>
      <Typography className={classes.h1} variant="h1" align="center">
        Fakebook
      </Typography>
    </Paper>

  )
}

export default Header
