import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  ListItemIcon,
  ListItemText,
  MenuList,
  MenuItem,
  Paper,
  Typography,
} from '@material-ui/core'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import AssignmentIcon from '@material-ui/icons/Assignment'
import BallotIcon from '@material-ui/icons/Ballot'
import ChatIcon from '@material-ui/icons/Chat'
import EventIcon from '@material-ui/icons/Event'
import GroupIcon from '@material-ui/icons/Group'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(3),
  },
  h1: {
    fontWeight: 500,
  },
  menuItem: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },
  },
  primary: {},
  icon: {},
}))

const Menu = () => {
  const classes = useStyles()

  return (
    <Paper className={classes.paper}>
      <Typography variant="h5">Navigation</Typography>
      <MenuList>
        <MenuItem className={classes.menuItem}>
          <ListItemIcon className={classes.icon}>
            <AccountBoxIcon/>
          </ListItemIcon>
          <ListItemText classes={{ primary: classes.primary }} inset primary="My Profile"/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <ListItemIcon className={classes.icon}>
            <ChatIcon/>
          </ListItemIcon>
          <ListItemText classes={{ primary: classes.primary }} inset primary="Messenger"/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <ListItemIcon className={classes.icon}>
            <AssignmentIcon/>
          </ListItemIcon>
          <ListItemText classes={{ primary: classes.primary }} inset primary="Pages"/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <ListItemIcon className={classes.icon}>
            <GroupIcon/>
          </ListItemIcon>
          <ListItemText classes={{ primary: classes.primary }} inset primary="Groups"/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <ListItemIcon className={classes.icon}>
            <EventIcon/>
          </ListItemIcon>
          <ListItemText classes={{ primary: classes.primary }} inset primary="Events"/>
        </MenuItem>
        <MenuItem className={classes.menuItem}>
          <ListItemIcon className={classes.icon}>
            <BallotIcon/>
          </ListItemIcon>
          <ListItemText classes={{ primary: classes.primary }} inset primary="Recommendations"/>
        </MenuItem>
      </MenuList>
    </Paper>
  )
}

export default Menu
