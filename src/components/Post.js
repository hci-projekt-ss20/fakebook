import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Avatar,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Typography,
} from '@material-ui/core/'
import FavoriteIcon from '@material-ui/icons/Favorite'
import ShareIcon from '@material-ui/icons/Share'
import MoreVertIcon from '@material-ui/icons/MoreVert'

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: '20px',
    backgroundColor: theme.palette.secondary.light,
    color: theme.palette.common.black,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main,
  },
}))

const Post = ({ author, date, image, text }) => {
  const classes = useStyles()

  return (
    <Card className={classes.root} id={`post-${author}-${date}`}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            {author.substr(0, 1)}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon/>
          </IconButton>
        }
        title={author}
        subheader={date}
      />
      {image ?
        <CardMedia
          className={classes.media}
          image={image}
          title="Paella dish"
        />
        :
        ''
      }
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {text}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="like">
          <FavoriteIcon/>
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon/>
        </IconButton>
      </CardActions>
    </Card>
  )
}

export default Post
