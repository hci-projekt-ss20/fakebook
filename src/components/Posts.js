import React from 'react'
import Post from './Post'
import { makeStyles } from '@material-ui/core/styles'
import { Paper } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
  },
}))

const Posts = ({ posts }) => {
  const classes = useStyles()

  return (
    <Paper className={classes.paper}>
      {posts.map((post, index) =>
        <Post
          key={index}
          author={post.author}
          date={post.date}
          image={post.image}
          text={post.text}
        />
      )}
    </Paper>
  )
}

export default Posts
