const contacts = [
  'Alice',
  'Bob',
  'Parmesh',
  'Regina',
  'Taraneh',
  'Khashayar',
  'Sunitha',
  'Matthew',
  'Ashley',
  'Kayleigh',
  'Alexander',
  'Sebastian',
  'Marcus',
  'Mai Thi',
  'Dingxiang',
  'Kane',
  'Kieran',
  'Kazunori',
  'Kiriko',
  'Samuel',
  'Majima',
  'Makoto',
  'Nadine',
  'Vincenzo',
  'Carla',
  'Claudia',
  'Anja',
  'Anna',
  'Johannes',
  'Thomas',
].sort()

const posts = [
  {
    image: false,
    text: 'My favorite pseudo-latin sentence is definitely "lorem ipsum dolor sit amet".',
  },
  {
    image: 'https://material-ui.com/static/images/cards/paella.jpg',
    text: 'This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.'
  },
  {
    image: 'https://material-ui.com/static/images/cards/contemplative-reptile.jpg',
    text: 'Check out this awesome looking reptile!'
  },
  {
    image: false,
    text: 'I think Formula 1 should stop asking their fans how to make the sport more entertaining. They\'re asking those who are already tuning in. Instead they should ask those who find F1 boring.'
  },
  {
    image: false,
    text: 'Visual Studio Code is already pretty great for JavaScript development. WebStorm is even better though!'
  },
  {
    image: false,
    text: 'This summer is killing me. I don\'t mind the heat as much, but that damned humidity man...'
  },
  {
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Before_Machu_Picchu.jpg/1280px-Before_Machu_Picchu.jpg',
    text: 'I started using this dating up and it seems like everyone there has been to Peru.'
  },
  {
    image: false,
    text: 'My cat\'s breath smells like cat food.'
  },
  {
    image: false,
    text: 'Every new season of Deadliest Catch seems identical to the one before. But I still love watching that show!'
  }
]

const randomContact = () => contacts[Math.floor(Math.random() * contacts.length)]

const randomPost = () => posts[Math.floor(Math.random() * posts.length)]

module.exports = {
  contacts,
  posts,
  randomContact,
  randomPost,
}
