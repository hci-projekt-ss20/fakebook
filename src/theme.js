import { createMuiTheme } from '@material-ui/core/styles'
import {
  green,
  grey,
  indigo,
  orange,
  teal,
  yellow,
} from '@material-ui/core/colors'

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: indigo,
    secondary: {
      light: orange['50'],
      main: orange.A400,
      dark: orange['900'],
    },
    info: teal,
    background: {
      default: grey['100'],
      paper: indigo['300']
    },
    // custom color definitions
    contactOnline: green[300],
    contactAway: yellow[700],
  }
})

export default theme
