import fixtures from '../data/fixtures'

export const contactsOnline = fixtures.contacts.filter(() => Math.random() < 0.4)

export const contactsAway = fixtures.contacts
  .filter(contact => !contactsOnline.includes(contact))
  .filter(() => Math.random() < 0.3)

export const posts = (() => {
  const randomPosts = []
  let datetime = new Date()
  for (let i = 0; i < 25; i++) {
    const post = fixtures.randomPost()
    randomPosts.push({
      author: fixtures.randomContact(),
      date: datetime.toLocaleString(),
      image: post.image,
      text: post.text,
    })
    datetime = new Date(datetime - Math.floor(Math.random() * 5400000))
  }
  return randomPosts
})()
